const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const bodyParse = require("body-parser");
const cheerio = require("cheerio");
const request = require("request");
var artoo = require('artoo-js');
var moment = require('moment');
var $ = require("jquery");

//database
//mongoose.connect("mongodb://admin:<password>@ds115283.mlab.com:15283/graysonline");
mongoose.connect("mongodb://localhost/grays");
let db = mongoose.connection;

db.once("open", function() {
  console.log("Connect to the database")
})

db.on("error", function(err) {
  console.log(err);
})
const app = express();

let User = require("./models/users")

app.use(bodyParse.urlencoded({
  extended: false
}));
app.use(bodyParse.json());

app.use(express.static(path.join(__dirname, 'public')))

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug')

var carBrands = ["Holden", "Subaru", "Toyota", "Jeep", "Mazda", "Ford", "Kia", "Suzuki", "Volkswagen", "Mitsubishi", "Nissan", "BMW"]
var cars = [];
var gumCars = []
var gumtreeCars = [];
var $minPrice = "";
function compare(brand, model, year, i){
    request('https://www.carsguide.com.au/' + brand + '/' + model + '/price/' + year, (error, response, html) => {
      console.log('https://www.carsguide.com.au/' + brand + '/' + model + '/price/' + year)
      const $ = cheerio.load(html);
      $minPrice = $(".makeModelYear--priceMedianContent .price").text().split("$")[1]
      console.log("Car Guides Price: " + $minPrice)
      gumtreeCars[i].recPrice = $minPrice;
    })
}
function removeDuds(){
  for(var i = 0; i <= gumtreeCars.length; i++){
    console.log("Title: " + gumtreeCars[i].title)
    if(gumtreeCars[i].title != null){
      gumCars.push(gumtreeCars[i]);
    }else{
      return
    }
  }
}
//https://www.carsguide.com.au/holden/commodore/price/2004
function scrapeGumtreeCars(){
  for(var page = 1; page <= 20; page++){
    request('https://www.gumtree.com.au/s-cars-vans-utes/brisbane/page-' + page + '/c18320l3005721?sort=price_asc', (error, response, html) => {
      if (!error && response.statusCode == 200) {
        const $ = cheerio.load(html);
        $(".panel-body .user-ad-row").each((i, el) => {
          var scrapeEach = new Promise(function(resolve, reject) {
            var carr = $(el).attr("aria-label").toString();
            setTimeout(function() {
              var price = carr.split("Price: ")[1].split(" .")[0]
              if(price != "not listed" || price != "not listed " || price != " not listed" && price[0] == "$" && price.length <= 7){
                var id = $(el).attr("id")
                gumtreeCars.push({id: id})
                if(price.length >= 9){
                  price = price.split(" ")[0]
                }
                if(carr[0] == "2" || carr[0] == "1"){
                  var link = $(el).attr("href");
                  var model = carr.split(".")[0].split(" ")[2]
                  var brand = carr.split(".")[0].split(" ")[1]
                  var year = carr.split(" ")[0].toString();
                  var time = carr.split("Ad listed ")[1].split(".")[0]
                  var title = year + " " + brand + " " + model;
                  var location = carr.split("Location: ")[1].split(".")[0];
                  carBrands.forEach((b) => {
                    var assumption = carr.split(" ")[1].split(" ")[0];
                    if(assumption != "undefined" && assumption == b){
                      gumtreeCars[i].location = location;
                      gumtreeCars[i].brand = brand;
                      gumtreeCars[i].title = title;
                      gumtreeCars[i].price = price;
                      gumtreeCars[i].time = time;
                      gumtreeCars[i].link = link;
                      gumtreeCars[i].year = year;
                      gumtreeCars[i].model = model;
                      resolve(gumtreeCars[i])
                      compare(brand, model, year, i);
                    }else{
                      return
                    }
                  })
                }
              }
            }, 1000);
          })
          scrapeEach.then(function(value) {
            console.log(value);
          });
        })
      }
    })
  }
}

scrapeGumtreeCars();

function scrapeGraysCars(){
  request('http://www.graysonline.com/automotive-trucks-and-marine/motor-vehiclesmotor-cycles?location=qld&page-size=100&sort=close-time-asc', (error, response, html) => {
    cars = [];
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html);
      var count;
      var price;
      var time;
      $(".price").each((i, el) => {
        cars.push({
          id: i
        });
      })
      $('.s3c-top img').each((i, el) => {
        var title = $(el).attr("title");
        cars[i].title = title.toString();
      })
      $(".price").each((i, el) => {
        var price = $(el).find('.prod-price').text().replace(/\s\s+/g, '');
        cars[i].price = price;
      })

      $(".lot-closing-datetime").each((sdf, el) => {
        var title = $(el).attr('value').toString();
        title = title.split(" ");
        var stringArray = new Array();
        for(var i =0; i < title.length; i++){
          stringArray.push(title[i]);
          if(i != title.length-1){
            stringArray.push(" ");
          }
        }
        var date = title[1];
        var time = title[4];
        time  = time.split(":");
        var min = time[1].split(":")
        var time2 = parseInt(time[0]) + 10;
        var end =  time2 + ":" + min[0];
        var d = new Date();
        var now = d.getHours() +":"+ d.getMinutes();
        var a = moment(now, 'HH:mm');
        var b = moment(end, 'HH:mm');
        var c = b.diff(a, 'hours', true)
        var result = Math.round(60/100 * Math.round(c*100))
        cars[sdf].time = result;
      })

      $('.s3c-top img').each((i, el) => {
        var img = $(el).attr("data-src");
        cars[i].image = img.toString();
      })
      $('.s3c-top a').each((i, el) => {
        var link = $(el).attr("href");
        cars[i].link = link.toString();
      })
      console.log(cars.length + " cars found...");
    } else {
      return error;
    }
  })
}

function saveSearch(){
  cars.forEach((car) => {
    timer = 0.5;
    let user = new User();
    user.title = car.title
    user.time = car.time
    user.price = car.price
    user.link = car.link
    user.image = car.image
    console.log("saved: " + car.title)
    user.save();
  })
}

async function asyncCall() {
  setInterval(function() {
    if(cars[0].time <= 10 && cars[0].time >= -50){
      console.log(cars[0].time + " minutes untill the next showdown... 1 minute intervals will occur")
      saveSearch();
    }else{
      console.log("No cars ending in the next 10 mins")
    }
  }, 60 * 1000 * 1);
  setInterval(function() {
    if(cars[0].time < 60 && cars[0].time > 10){
      console.log(cars[0].time + " minutes untill the next showdown... 10 minute intervals will occur")
      saveSearch();
    }else{
      console.log("No cars ending in the next hour")
    }
  }, 60 * 1000 * 10);
  setInterval(function() {
    if(cars[0].time >= 60){
      console.log(cars[0].time + " minutes untill the next showdown... 1 hour intervals will occur")
      saveSearch();
    }else{
      console.log("No cars ending anytime soon")
    }
  }, 60 * 1000 * 60);
}

app.get("/", (req, res) => {
  User.find({}, (err, cars) => {
    res.render("cars", {
      cars: cars
    })
  })
})

app.get("/gumtree", (req, res) => {
  gumCars = [];
  res.render("gumtree", {
    cars: gumtreeCars
  })
})

const PORT = process.env.PORT || 3000
app.listen(PORT, function() {
  console.log("Web App started on port 3000...")
})
