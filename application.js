const cheerio = require("cheerio");
const request = require("request");

var cars = [];

request('http://www.graysonline.com/automotive-trucks-and-marine/motor-vehiclesmotor-cycles?location=qld&page-size=100&sort=close-time-asc', (error, response, html) => {
  if(!error && response.statusCode == 200){
    const $ = cheerio.load(html);
    var count;
    var price;
    var time;
    $(".price").each((i, el) => {
      cars.push({id: i});
    })
    $('.s3c-top img').each((i, el) => {
      cars[i].title = $(el).attr("title");
    })
    $(".price").each((i, el) => {
      cars[i].price = $(el).find('.prod-price').text().replace(/\s\s+/g, '')})

    $(".lot-closing-datetime").each((i, el) => {
      cars[i].time = $(el).attr('value')})

    $('.s3c-top img').each((i, el) => {
      cars[i].image = $(el).attr("data-src");
    })
    $('.s3c-top a').each((i, el) => {
      cars[i].link = $(el).attr("href");
    })
    console.log(cars)
    var d = new Date()
    console.log(d.getTime());
  }else{
    return error;
  }
});
