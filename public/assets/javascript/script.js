$(".btn").click(() => {
  $("#myModal").css("display", "block")
  $("body").css("overflow", "hidden")
})

function size(){
  console.log("Toggle Screen: " + $(window).width())
  if($(window).width() <= 980){
    $("body").addClass("mobile");
    $("body").removeClass("desktop");
  }
  if($(window).width() > 980){
    $("body").addClass("desktop");
    $("body").removeClass("mobile");
  }
}
$(window).resize(() => {
  size();
});
$("document").ready(() => {
  size();
})

$(".close").click(() => {
  $("#myModal").css("display", "none")
  $("body").css("overflow", "auto")
})
function scroll(){
  $("#cont1").css("animation", "scrollDelay .5s")
  console.log("scrolling")
}
