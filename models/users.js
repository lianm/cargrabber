let mongoose = require("mongoose");

let userSchema = mongoose.Schema({
  carId:{
    type: String
  },
  title:{
    type: String
  },
  price:{
    type: String
  },
  time:{
    type: String
  },
  link:{
    type: String
  },
  image:{
    type: String
  }
})

let User = module.exports = mongoose.model("User", userSchema)
